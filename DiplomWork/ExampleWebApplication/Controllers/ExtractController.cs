﻿using ExampleWebApplication.Models;
using Microsoft.AspNet.Identity.Owin;
using StegoSVG.API;
using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;

namespace ExampleWebApplication.Controllers
{
    [Authorize]
    public class ExtractController : Controller
    {
        ApplicationDbContext db = new ApplicationDbContext();

        [HttpGet]
        public ActionResult Extract()
        {
            return View();
        }
        public ActionResult ExtractMessage(ExtractViewModel extract)
        {
            if (extract != null)
            {
                if (extract.File != null)
                {
                    try
                    {
                        StegoSvgAPI api = new StegoSvgAPI();
                        api.Load(extract.File.InputStream);
                        string message = api.ExtractMessage();
                        message = message.Remove(message.Length - 1, 1);
                        var userManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
                        Crypto.RSA rsa = new Crypto.RSA();
                        switch (extract.SelMode)
                        {
                            case "crypt":
                                {
                                    var data = new byte[extract.File.InputStream.Length];
                                    extract.File.InputStream.Position = 0;
                                    extract.File.InputStream.Read(data, 0, data.Length);
                                    var byteHash = new SHA256Managed().ComputeHash(data);
                                    var svgHashString = Convert.ToBase64String(byteHash);
                                    var info = db.AppLog.Include(u => u.User).Where(h => h.SvgHash == svgHashString).OrderByDescending(d => d.Date).FirstOrDefault();
                                    return PartialView("SignMessage",
                                        new UnSignMessage
                                        {
                                            Verify = "Сообщение было зашифровано.",
                                            Message = DecryptMessage(message, extract.Password, extract.FileKey, userManager),
                                            MessageAuthor = info == null ? "Неизвестен." : info.User.Email,
                                            DateSign = info != null ? info.Date : new DateTime(),
                                            IsChangeFile = info == null ? "Да" : "Нет"
                                        });
                                }
                            case "sign":
                                {
                                    UnSignMessage unSignMessage = CheckSign(userManager, extract.SenderEmail, message, extract.File);
                                    return PartialView("SignMessage", unSignMessage);
                                }
                            case "signCrypt":
                                {
                                    message = DecryptMessage(
                                            message,
                                            extract.Password,
                                            extract.FileKey,
                                            userManager);
                                    UnSignMessage unSignMessage = CheckSign(userManager, extract.SenderEmail, message, extract.File);
                                    return PartialView("SignMessage", unSignMessage);
                                }
                            case "open":
                                {
                                    var data = new byte[extract.File.InputStream.Length];
                                    extract.File.InputStream.Position = 0;
                                    extract.File.InputStream.Read(data, 0, data.Length);
                                    var byteHash = new SHA256Managed().ComputeHash(data);
                                    var svgHashString = Convert.ToBase64String(byteHash);
                                    var info = db.AppLog
                                        .Include(u => u.User)
                                        .Where(h => h.SvgHash == svgHashString).OrderByDescending(d => d.Date).FirstOrDefault();
                                    var y = db.AppLog.ToList();
                                    return PartialView("SignMessage",
                                        new UnSignMessage
                                        {
                                            Verify = "Сообщение было передано открытым текстом.",
                                            Message = message,
                                            MessageAuthor = info == null ? "Неизвестен." : info.User.Email,
                                            DateSign = info != null ? info.Date : new DateTime(),
                                            IsChangeFile = info == null ? "Да" : "Нет"
                                        });
                                }
                        }
                    }
                    catch (Exception e)
                    {
                        return PartialView("GettingMessage", e.Message);
                    }
                }
                return PartialView("GettingMessage", "Вы не указали файл.");
            }
            return PartialView("GettingMessage", "Ошибка при передаче формы на сервер.");
        }

        private string DecryptMessage(string message, string password, HttpPostedFileBase fileKey, ApplicationUserManager userManager)
        {
            Crypto.RSA rsa = new Crypto.RSA();
            if (password != null && password != "")
            {
                var user = userManager.FindByNameAsync(User.Identity.Name).Result;
                if (user.EncryptedPrivateKey == "")
                {
                    return "Вы не сохраняли секретный ключ в базе. Укажите файл с ключом.";
                }
                string decryptedKey = Crypto.RijndaelAlg.Decrypt(user.EncryptedPrivateKey, password);
                message = rsa.Decrypt(decryptedKey, message);
                return message;
            }
            else if (fileKey != null)
            {
                byte[] array = new byte[fileKey.InputStream.Length];
                fileKey.InputStream.Read(array, 0, array.Length);
                var decryptedKey = Encoding.Default.GetString(array);
                message = rsa.Decrypt(decryptedKey, message);
                return message;
            }
            else
            {
                return "Ошибка при получении секретного ключа. Возможно неверный пароль или указанный файл с ключом некорректен.";
            }
        }
        private UnSignMessage CheckSign(ApplicationUserManager userManager, string senderEmail, string message, HttpPostedFileBase fileForHash)
        {
            Crypto.RSA rsa = new Crypto.RSA();
            var receiverUser = userManager.FindByEmailAsync(senderEmail).Result;
            var (receiveMessage, verify) = rsa.UnSign(receiverUser.PublicKey, message);
            var data = new byte[fileForHash.InputStream.Length];
            fileForHash.InputStream.Position = 0;
            fileForHash.InputStream.Read(data, 0, data.Length);
            var byteHash = new SHA256Managed().ComputeHash(data);
            var svgHashString = Convert.ToBase64String(byteHash);
            var info = db.AppLog.Include(u => u.User).Where(h => h.SvgHash == svgHashString).OrderByDescending(d => d.Date).FirstOrDefault();
            return new UnSignMessage { Verify = verify ? "Да" : "Нет", Message = receiveMessage, MessageAuthor = receiverUser.Email, DateSign = info != null ? info.Date : new DateTime(), IsChangeFile = info == null ? "Да" : "Нет" };
        }

    }
}