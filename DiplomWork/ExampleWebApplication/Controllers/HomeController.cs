﻿using ExampleWebApplication.Models;
using StegoSVG.API;
using System.Web;
using System.Web.Mvc;

namespace ExampleWebApplication.Controllers
{
    public class HomeController : Controller
    {
        ApplicationDbContext db = new ApplicationDbContext();

        [Authorize]
        public ActionResult InitApi(HttpPostedFileBase File)
        {
            try
            {
                Session["api"] = new StegoSvgAPI();
                return PartialView("AnalysesResult", ((StegoSvgAPI)Session["api"]).Load(File.InputStream));
            }
            catch
            {
                return PartialView("GettingMessage", "Проверьте корректность файла, возможно вы ошиблись при выборе.");
            }
        }

        public ActionResult Index()
        {
            return View();
        }
    }
}