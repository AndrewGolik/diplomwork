﻿using ExampleWebApplication.Models;
using Microsoft.AspNet.Identity.Owin;
using StegoSVG.API;
using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace ExampleWebApplication.Controllers
{
    [Authorize]
    public class PreciptController : Controller
    {
        ApplicationDbContext db = new ApplicationDbContext();

        [HttpGet]
        public ActionResult Precipt()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Precipt(PreciptViewModel precipt)
        {
            Session["preciptMemory"] = new MemoryStream();
            if (precipt.Message == String.Empty)
            {
                return PartialView("GettingMessage", "Укажите сообщение.");
            }
            if (precipt != null)
            {
                var userManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
                Crypto.RSA rsa = new Crypto.RSA();
                switch (precipt.SelMode)
                {
                    case "crypt":
                        {
                            try
                            {
                                precipt.Message = CryptMessage(precipt.ReceiverEmail, precipt.Message, userManager);
                                break;
                            }
                            catch
                            {
                                return PartialView("GettingMessage", "Ошибка при получении публичного ключа. Возможно введен незарегистрированный пользователь.");
                            }
                        }
                    case "sign":
                        {
                            var (trySign, sign) = TrySign(precipt.Message, precipt.Password, precipt.FileKey, userManager);
                            if (trySign)
                            {
                                precipt.Message = sign;
                            }
                            else
                            {
                                return PartialView("GettingMessage", "Ошибка при получении секретного ключа. Возможно неверный пароль или указанный файл с ключом некорректен.");
                            }
                            break;
                        }
                    case "signCrypt":
                        {
                            var (trySign, sign) = TrySign(precipt.Message, precipt.Password, precipt.FileKey, userManager);
                            if (trySign)
                            {
                                precipt.Message = sign;
                            }
                            else
                            {
                                return PartialView("GettingMessage", "Ошибка при получении секретного ключа. Возможно неверный пароль или указанный файл с ключом некорректен.");
                            }
                            try
                            {
                                precipt.Message = CryptMessage(precipt.ReceiverEmail, precipt.Message, userManager);
                                break;
                            }
                            catch
                            {
                                return PartialView("GettingMessage", "Ошибка при получении публичного ключа. Возможно введен незарегистрированный пользователь.");
                            }
                        }
                }
                try
                {
                    Session["preciptMemory"] = ((StegoSvgAPI)Session["api"]).PrecipitationMessage(precipt.Message);
                    SendMailViewModel beforeMail = new SendMailViewModel
                    {
                        ReceiverEmail = precipt.ReceiverEmail,
                        SenderEmail = User.Identity.Name
                    };

                    SaveLogDb(precipt.SelMode, userManager);
                    return PartialView("SendMail", beforeMail);
                }
                catch (Exception e)
                {
                    return PartialView("GettingMessage", e.Message);
                }
            }
            else
            {
                return PartialView("GettingMessage", "Ошибка при передаче формы на сервер.");
            }
        }

        [HttpGet]
        public FileResult DownloadFile()
        {
            return File(((MemoryStream)Session["preciptMemory"]).ToArray(), "text/xml", "test.svg");
        }

        [HttpGet]
        public FileContentResult GetFileWithPreciptMessage()
        {
            return File(
                ((MemoryStream)Session["preciptMemory"]).ToArray(),
                MediaTypeNames.Application.Octet
                );
        }

        [HttpPost]
        public ActionResult SendMail(SendMailViewModel sendMail)
        {
            try
            {
                var preciptMemory = ((MemoryStream)Session["preciptMemory"]);
                MailAddress from = new MailAddress(sendMail.SenderEmail, sendMail.SenderEmail);
                MailAddress to = new MailAddress(sendMail.ReceiverEmail);
                MailMessage mailMessage = new MailMessage(from, to)
                {
                    Subject = sendMail.Subject,
                    Body = sendMail.TextLetter,
                    IsBodyHtml = true
                };
                preciptMemory.Flush();
                preciptMemory.Seek(0, SeekOrigin.Begin);
                ContentType contentType = new ContentType
                {
                    MediaType = MediaTypeNames.Application.Octet
                };
                if (sendMail.FileName == null) sendMail.FileName = "NewFile";
                contentType.Name = sendMail.FileName.Contains(".svg") ? sendMail.FileName : sendMail.FileName + ".svg";
                Attachment attach = new Attachment(preciptMemory, contentType);
                mailMessage.Attachments.Add(attach);
                SmtpClient smtp = new SmtpClient("smtp.gmail.com", 587)
                {
                    Credentials = new NetworkCredential("stegosvgserver@gmail.com", "bndyt512"),
                    EnableSsl = true
                };
                smtp.Send(mailMessage);
                return PartialView("GettingMessage", "Письмо успешно отправлено");
            }
            catch
            {
                return PartialView("GettingMessage", "Произошла ошибка");
            }
        }

        private string CryptMessage(string userEmail, string message, ApplicationUserManager userManager)
        {
            var receiverUser = userManager.FindByEmailAsync(userEmail).Result;
            Crypto.RSA rsa = new Crypto.RSA();
            return rsa.Encrypt(receiverUser.PublicKey, message);
        }

        private (bool, string) TrySign(string message, string password, HttpPostedFileBase fileKey, ApplicationUserManager userManager)
        {
            var (correct, key) = IsHasKeyInDB(password, userManager);
            if (correct)
            {
                message = DecryptKeyAndSignMessage(key, password, message);
            }
            else if (fileKey != null)
            {
                message = DecryptKeyAndSignMessage(fileKey, message);
            }
            else
            {
                return (false, String.Empty);
            }
            return (true, message);
        }
        private (bool, string) IsHasKeyInDB(string password, ApplicationUserManager userManager)
        {
            var user = userManager.FindByNameAsync(User.Identity.Name).Result;
            if (user.EncryptedPrivateKey == String.Empty)
            {
                return (false, String.Empty);
            }
            return (true, user.EncryptedPrivateKey);
        }
        private string DecryptKeyAndSignMessage(string encryptKey, string password, string message)
        {
            string decryptedKey = Crypto.RijndaelAlg.Decrypt(encryptKey, password);
            return SigningMessage(decryptedKey, message);
        }
        private string DecryptKeyAndSignMessage(HttpPostedFileBase fileKey, string message)
        {
            byte[] array = new byte[fileKey.InputStream.Length];
            fileKey.InputStream.Read(array, 0, array.Length);
            string decryptedKey = Encoding.Default.GetString(array);
            return SigningMessage(decryptedKey, message);
        }
        private string SigningMessage(string decryptedKey, string message)
        {
            Crypto.RSA rsa = new Crypto.RSA();
            return message + rsa.Sign(decryptedKey, message);
        }

        private void SaveLogDb(string operation, ApplicationUserManager userManager)
        {
            var preciptMemory = ((MemoryStream)Session["preciptMemory"]);
            var data = new byte[preciptMemory.Length];
            preciptMemory.Position = 0;
            preciptMemory.Read(data, 0, data.Length);
            var byteHash = new SHA256Managed().ComputeHash(data);
            var svgHashString = Convert.ToBase64String(byteHash);
            var author = db.Users.Where(n => n.UserName == User.Identity.Name).FirstOrDefault();
            var log = new AppLogDb
            {
                User = author,
                SvgHash = svgHashString,
                Operation = operation,
                Date = DateTime.Now
            };
            db.AppLog.Add(log);
            db.SaveChangesAsync();
        }
    }
}