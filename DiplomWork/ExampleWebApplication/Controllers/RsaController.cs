﻿using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Web.Mvc;

namespace ExampleWebApplication.Controllers
{
    public class RsaController : Controller
    {
        // GET: Rsa
        public JsonResult GenerateRsaKey()
        {
            RSA rsa = new RSACryptoServiceProvider(1024);
            string privateKey = rsa.ToXmlString(true);
            string publicKey = rsa.ToXmlString(false);
            IEnumerable<string> keys = new string[]
            {
                publicKey,
                privateKey
            };
            return Json(keys, JsonRequestBehavior.AllowGet);
        }
        [HttpGet, ValidateInput(false)]
        public FileResult DownloadKey(string keyTxt)
        {
            keyTxt = keyTxt.Replace(" ", "+");
            MemoryStream s = new MemoryStream(Encoding.Default.GetBytes(keyTxt));
            return File(s.ToArray(), "application/txt", "privateKey.txt");
        }
    }
}