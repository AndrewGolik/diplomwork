﻿using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;

namespace ExampleWebApplication.Crypto
{
    public class RSA
    {
        public string Encrypt(string key, string message)
        {
            RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
            rsa.FromXmlString(key);
            int intCount = message.Length / 117;
            int doubleCount = message.Length % 117;
            string returnString = String.Empty;
            for (int i = 0, y = 0; i < intCount; i++)
            {
                string tmp = message.Substring(y, 117);
                y += 117;
                returnString += Convert.ToBase64String(rsa.Encrypt(Encoding.GetEncoding(1251).GetBytes(tmp), false));
            }
            if (doubleCount != 0)
            {
                string tmp = message.Substring(message.Length - doubleCount, doubleCount);
                returnString += Convert.ToBase64String(rsa.Encrypt(Encoding.GetEncoding(1251).GetBytes(tmp), false));
            }

            return returnString;

        }
        public string Decrypt(string key, string message)
        {
            RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
            rsa.FromXmlString(key);
            var splitMessage = Regex.Split(message, @"(?<=\G.{172})");
            var listSplitMessage = splitMessage.ToList();
            if (listSplitMessage.Last() == "")
            {
                listSplitMessage.Remove(listSplitMessage.Last());
            }
            string returnString = String.Empty;
            foreach (var part in listSplitMessage)
            {
                var encM = Convert.FromBase64String(part);
                var decryptPart = rsa.Decrypt(encM, false);
                returnString += Encoding.GetEncoding(1251).GetString(decryptPart);
            }
            return returnString;
        }
        public string Sign(string privateKeyXml, string message)
        {
            ASCIIEncoding ByteConverter = new ASCIIEncoding();
            byte[] original = ByteConverter.GetBytes(message);
            RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
            rsa.FromXmlString(privateKeyXml);
            var signedMessage = rsa.SignData(original, new SHA256CryptoServiceProvider());
            var stringSign = Convert.ToBase64String(signedMessage);
            return stringSign;
        }
        public (string, bool) UnSign(string publicKeyXml, string message)
        {
            ASCIIEncoding ByteConverter = new ASCIIEncoding();
            string sign = message.Substring(message.Length - 172, 172);
            string originMess = message.Substring(0, message.Length - 172);
            byte[] origData = ByteConverter.GetBytes(originMess);
            byte[] signData = Convert.FromBase64String(sign);
            RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
            rsa.FromXmlString(publicKeyXml);
            bool verify = rsa.VerifyData(origData, new SHA256CryptoServiceProvider(), signData);
            message = originMess;
            return (originMess, verify);
        }
    }
}