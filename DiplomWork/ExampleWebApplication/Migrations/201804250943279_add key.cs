namespace ExampleWebApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addkey : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "Key", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "Key");
        }
    }
}
