namespace ExampleWebApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class delkey : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.AspNetUsers", "Key");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AspNetUsers", "Key", c => c.String());
        }
    }
}
