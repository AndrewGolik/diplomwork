namespace ExampleWebApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Addlog : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AppLog",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserName = c.String(nullable: false),
                        SvgHash = c.String(nullable: false),
                        Operation = c.String(nullable: false),
                        Date = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.AppLog");
        }
    }
}
