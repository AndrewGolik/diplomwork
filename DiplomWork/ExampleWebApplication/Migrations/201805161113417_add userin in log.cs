namespace ExampleWebApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class adduserininlog : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AppLog", "ApplicationUserId", c => c.Int(nullable: false));
            DropColumn("dbo.AppLog", "UserName");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AppLog", "UserName", c => c.String(nullable: false));
            DropColumn("dbo.AppLog", "ApplicationUserId");
        }
    }
}
