namespace ExampleWebApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateappLogInDb : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AppLog", "User_Id", c => c.String(nullable: false, maxLength: 128));
            CreateIndex("dbo.AppLog", "User_Id");
            AddForeignKey("dbo.AppLog", "User_Id", "dbo.AspNetUsers", "Id", cascadeDelete: true);
            DropColumn("dbo.AppLog", "ApplicationUserId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AppLog", "ApplicationUserId", c => c.Int(nullable: false));
            DropForeignKey("dbo.AppLog", "User_Id", "dbo.AspNetUsers");
            DropIndex("dbo.AppLog", new[] { "User_Id" });
            DropColumn("dbo.AppLog", "User_Id");
        }
    }
}
