﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ExampleWebApplication.Models
{
    [Table("AppLog")]
    public class AppLogDb
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        public ApplicationUser User { get; set; }

        [Required]
        public string SvgHash { get; set; }

        [Required]
        public string Operation { get; set; }

        [Required]
        public DateTime Date { get; set; }
    }
}