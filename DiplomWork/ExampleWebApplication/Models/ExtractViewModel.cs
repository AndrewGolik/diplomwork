﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace ExampleWebApplication.Models
{
    public class ExtractViewModel
    {
        [Display(Name = "Выберите файл")]
        public HttpPostedFileBase File { get; set; }
        [Display(Name = "Или укажите файл с секретным ключом, полученным при регистрации")]
        public HttpPostedFileBase FileKey { get; set; }
        public string SelMode { get; set; }
        [Display(Name = "Email отправителя")]
        public string SenderEmail { get; set; }
        [Display(Name = "Введите пароль для доступа к секретному ключу")]
        public string Password { get; set; }
    }
}