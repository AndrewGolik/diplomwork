﻿using System.ComponentModel.DataAnnotations;
using System.Web;

namespace ExampleWebApplication.Models
{
    public class PreciptViewModel
    {
        [Required(ErrorMessage ="Укажите файл.")]
        [Display(Name = "Укажите файл для осаждения")]
        public HttpPostedFileBase File { get; set; }
        [Display(Name = "Или укажите файл с секретным ключом, полученным при регистрации")]
        public HttpPostedFileBase FileKey { get; set; }
        public string SelMode { get; set; }
        [Required(ErrorMessage = "Укажите сообщение.")]
        [Display(Name = "Введите сообщение")]
        public string Message { get; set; }

        [Display(Name = "Введите Email получателя")]
        public string ReceiverEmail { get; set; }
        [Display(Name = "Введите пароль для доступа к секретному ключу")]
        public string Password { get; set; }
    }
}