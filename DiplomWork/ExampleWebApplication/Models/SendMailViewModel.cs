﻿using System.ComponentModel.DataAnnotations;

namespace ExampleWebApplication.Models
{
    public class SendMailViewModel
    {
        [Display(Name = "От: ")]
        public string SenderEmail { get; set; } = "";
        [Display(Name = "Кому: ")]
        public string ReceiverEmail { get; set; } = "";
        [Display(Name = "Тема: ")]
        public string Subject { get; set; } = "";
        [Display(Name = "Текст письма: ")]
        public string TextLetter { get; set; } = "";
        [Display(Name = "Имя файла во вложении: ")]
        public string FileName { get; set; } = "";
    }
}