﻿using System;

namespace ExampleWebApplication.Models
{
    public class UnSignMessage
    {
        public string Verify { get; set; }
        public string Message { get; set; }
        public string MessageAuthor { get; set; }
        public DateTime DateSign { get; set; }
        public string IsChangeFile { get; set; }
    }
}