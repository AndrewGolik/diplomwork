﻿var selectedValue = "";
var errorMessage = "";


$("#uploadFile").change(function () {
    $("#gettingData").show();
    $('#results').hide();
    var input = this.files[0];
    if (input.type.match('image/svg*')) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#img-preview').attr('src', e.target.result);
            $('#img-preview').show();
        }
        reader.readAsDataURL(input);
        $("#extractFormWithoutFile").show();
    } else {
        $('#errorPrecipt').show();
        $('#errorPrecipt').text("Выбранный файл не является файлом SVG");
    }
});

$("input[name=SelMode]").click(function () {
    var value = $(this).val();
    selectedValue = value;
    if (value == "sign" || value == "signCrypt") {
        $("#senderEmailDiv").show();
    } else {
        $("#senderEmailDiv").hide();
    }
    if (value == "crypt" || value == "signCrypt") {
        $("#passwordDiv").show();
    } else {
        $("#passwordDiv").hide();
    }
});

$("#form0").submit(function (event) {
    event.stopPropagation();
    event.preventDefault();
    $('#img-preview').hide();
    if (!validation()) {
        $("#gettingData").text("Ошибка при выполнении");
        $('#errorPrecipt').show();
        $('#errorPrecipt').text(errorMessage);
    } else {
        $("#loading").show();
        $('#errorPrecipt').text("");
        var $that = $(this);
        var data = new FormData($that.get(0));
        $("#gettingData").text("Полученные результаты");
        $('#results').html("Идет обработка данных...");
        $.ajax({
            type: 'POST',
            url: '/Extract/ExtractMessage',
            processData: false,
            contentType: false,
            data: data,
            success: function (data) {
                $("#loading").hide();
                $('#results').html(data);
                $('#results').show();
            },
            error: function (xhr, str) {
                $("#loading").hide();
                $('#results').html('Возникла ошибка: ' + str);
            }
        });
    }
});

function validation() {
    if (selectedValue == "sign") {
        if ($("#sender").val() == "") {
            errorMessage = "Введите email отправителя.";
            return false;
        }
    }
    if (selectedValue == "crypt") {
        check = $("#password").val() != "" || $("#uploadKey").val() != "";
        if (!check) {
            errorMessage = "Укажите пароль или файл с ключом."
            return false;
        }
    }
    if (selectedValue == "signCrypt") {
        var t = $("#sender").val();
        if ($("#sender").val() == "") {
            errorMessage = "Введите email отправителя.";
            return false;
        }
        check = $("#password").val() != "" || $("#uploadKey").val() != "";
        if (!check) {
            errorMessage = "Укажите пароль или файл с ключом."
            return false;
        }
    }
    return true;
}