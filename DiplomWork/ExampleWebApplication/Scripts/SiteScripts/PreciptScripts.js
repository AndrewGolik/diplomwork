﻿var maxLength;
var valid = false;
var selectedValue = "";
var errorMessage = "";
var mailData;
var res;

$("#uploadFile").change(function () {
    var input = this.files[0];
    var data = new FormData();
    data.append("File", this.files[0]);
    if (input.type.match('image/svg*')) {
        $("#loadingInitApi").show();
        $.ajax({
            type: 'POST',
            url: '/Home/InitApi',
            processData: false,
            contentType: false,
            data: data,
            success: function (data) {
                $('#errorPrecipt').hide();
                $("#loadingInitApi").hide();
                $("#analysesTitle").text("Результаты анализа");
                $('#analysis').html(data);
                $('#analysis').show();
                maxLength = $("#maxMessageCount").val();
                var reader = new FileReader();
                reader.onload = function (e) {
                    res = e.target.result;
                    $('#img-preview').attr('src', e.target.result);
                    $('#img-preview').show();
                }
                reader.readAsDataURL(input);
                $("#preciptFormWithoutFile").show();
            },
            error: function (xhr, str) {
                $("#loadingInitApi").hide();
            }
        });
    } else {
        $("#loadingInitApi").hide();
        $("#analysesTitle").text("Ошибка при выполнении");
        $('#errorPrecipt').show();
        $('#errorPrecipt').text("Выбранный файл не является файлом SVG");
    }
});

$("input[name=SelMode]").click(function () {
    var value = $(this).val();
    selectedValue = value;
    if (value == "sign" || value == "signCrypt") {
        $("#passwordDiv").show();
    } else {
        $("#passwordDiv").hide();
    }
});

$("#message").on("change", function () {
    $("#currentLength").text(this.value.length);
    if (this.value.length > maxLength) {
        $("#currentLength").css("color", "red");
        valid = false;
    } else {
        $("#currentLength").css("color", "green");
        valid = true;
    }
});

$("#preciptForm").submit(function (event) {
    event.stopPropagation();
    event.preventDefault();
    $('#img-preview').hide();
    if (!validation()) {
        $("#analysesTitle").text("Ошибка при выполнении");
        $('#analysis').html(errorMessage);
    } else {
        $("#loading").show();
        var $that = $(this);
        var data = new FormData($that.get(0));
        $('#analysis').html("Идет обработка данных...");
        $.ajax({
            type: 'POST',
            url: '/Precipt/Precipt',
            processData: false,
            contentType: false,
            data: data,
            success: function (data) {
                mailData = data;
                showImg();
                $('#analysis').html("Файл после осаждения");
                $("#continueBtn").show();
                
            },
            error: function (xhr, str) {
                $("#loading").hide();
                $("#analysesTitle").text("Ошибка при выполнении");
                $('#analysis').html(str);
            }
        });
    }
});

function contBtn() {
    $('#img-preview').hide();
    $("#analysesTitle").text("Отправка письма");
    $('#analysis').html(mailData);
    $('#analysis').show();
    $("#continueBtn").hide();
};

function showImg() {
    $.ajax({
        type: 'GET',
        url: '/Precipt/GetFileWithPreciptMessage',
        success: function (data) {
            $('#img-preview').show();
            $("#loading").hide();
        },
        error: function (err) {
            console.log(err);
            alert(err);
        }
    })
}

function validation() {
    if ($("#message").val() == "") {
        errorMessage = "Введите сообщение.";
        return false;
    }
    if (selectedValue == "crypt") {
        if ($("#receiver").val() == "") {
            errorMessage = "Введите получателя.";
            return false;
        }
    }
    if (selectedValue == "sign") {
        check = $("#password").val() != "" || $("#fileKey").val() != "";
        if (!check) {
            errorMessage = "Укажите пароль или файл с ключом."
            return false;
        }
    }
    if (selectedValue == "signCrypt") {
        if ($("#receiver").val() == "") {
            errorMessage = "Введите получателя.";
            return false;
        }
        check = $("#password").val() != "" || $("#fileKey").val() != "";
        if (!check) {
            errorMessage = "Укажите пароль или файл с ключом."
            return false;
        }
    }
    return true;
}