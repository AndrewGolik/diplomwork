﻿$(function () {
    $("#generateKeyBtn").click(function () {
        $.get("/Rsa/GenerateRsaKey", function (data) {
            $("#publicKey").val(data[0]);
            $("#privateKey").val(data[1]);
            $("#saveKeyAsFile").attr("href", "/Rsa/DownloadKey?keyTxt=" + data[1]);
            $("#registerContiniue").show();
            $("#btnDiv").hide();
        });
    });
});