﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ExampleWebApplication.Startup))]
namespace ExampleWebApplication
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
