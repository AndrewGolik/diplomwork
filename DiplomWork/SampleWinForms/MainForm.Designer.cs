﻿namespace SampleWinForms
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.loadFileBtn = new System.Windows.Forms.Button();
            this.filePathTextBox = new System.Windows.Forms.TextBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.precipitationRadio = new System.Windows.Forms.RadioButton();
            this.extractRadio = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.statusListBox = new System.Windows.Forms.ListBox();
            this.perciptPanel = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.newFileNameTextBox = new System.Windows.Forms.TextBox();
            this.messageLengthCounter = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.perceptMessageTextBox = new System.Windows.Forms.TextBox();
            this.perceptMessageBtn = new System.Windows.Forms.Button();
            this.ExtractPanel = new System.Windows.Forms.Panel();
            this.extractMessageBtn = new System.Windows.Forms.Button();
            this.restoreMessageTextBox = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.perciptPanel.SuspendLayout();
            this.ExtractPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // loadFileBtn
            // 
            this.loadFileBtn.Location = new System.Drawing.Point(15, 10);
            this.loadFileBtn.Name = "loadFileBtn";
            this.loadFileBtn.Size = new System.Drawing.Size(104, 23);
            this.loadFileBtn.TabIndex = 0;
            this.loadFileBtn.Text = "Загрузить файл";
            this.loadFileBtn.UseVisualStyleBackColor = true;
            this.loadFileBtn.Click += new System.EventHandler(this.loadFileBtn_Click);
            // 
            // filePathTextBox
            // 
            this.filePathTextBox.Location = new System.Drawing.Point(125, 12);
            this.filePathTextBox.Name = "filePathTextBox";
            this.filePathTextBox.Size = new System.Drawing.Size(317, 20);
            this.filePathTextBox.TabIndex = 1;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // precipitationRadio
            // 
            this.precipitationRadio.AutoSize = true;
            this.precipitationRadio.Checked = true;
            this.precipitationRadio.Location = new System.Drawing.Point(14, 19);
            this.precipitationRadio.Name = "precipitationRadio";
            this.precipitationRadio.Size = new System.Drawing.Size(83, 17);
            this.precipitationRadio.TabIndex = 8;
            this.precipitationRadio.TabStop = true;
            this.precipitationRadio.Text = "Осаждение";
            this.precipitationRadio.UseVisualStyleBackColor = true;
            this.precipitationRadio.CheckedChanged += new System.EventHandler(this.radio_CheckedChange);
            // 
            // extractRadio
            // 
            this.extractRadio.AutoSize = true;
            this.extractRadio.Location = new System.Drawing.Point(14, 42);
            this.extractRadio.Name = "extractRadio";
            this.extractRadio.Size = new System.Drawing.Size(86, 17);
            this.extractRadio.TabIndex = 9;
            this.extractRadio.Text = "Извлечение";
            this.extractRadio.UseVisualStyleBackColor = true;
            this.extractRadio.CheckedChanged += new System.EventHandler(this.radio_CheckedChange);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.extractRadio);
            this.groupBox1.Controls.Add(this.precipitationRadio);
            this.groupBox1.Location = new System.Drawing.Point(15, 39);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(204, 68);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Осаждение/Извлечение";
            // 
            // statusListBox
            // 
            this.statusListBox.FormattingEnabled = true;
            this.statusListBox.Location = new System.Drawing.Point(463, 12);
            this.statusListBox.Name = "statusListBox";
            this.statusListBox.Size = new System.Drawing.Size(340, 420);
            this.statusListBox.TabIndex = 11;
            // 
            // perciptPanel
            // 
            this.perciptPanel.Controls.Add(this.label2);
            this.perciptPanel.Controls.Add(this.newFileNameTextBox);
            this.perciptPanel.Controls.Add(this.messageLengthCounter);
            this.perciptPanel.Controls.Add(this.label1);
            this.perciptPanel.Controls.Add(this.perceptMessageTextBox);
            this.perciptPanel.Controls.Add(this.perceptMessageBtn);
            this.perciptPanel.Location = new System.Drawing.Point(15, 113);
            this.perciptPanel.Name = "perciptPanel";
            this.perciptPanel.Size = new System.Drawing.Size(427, 316);
            this.perciptPanel.TabIndex = 12;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(20, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 13);
            this.label2.TabIndex = 24;
            this.label2.Text = "New file name:";
            // 
            // newFileNameTextBox
            // 
            this.newFileNameTextBox.Location = new System.Drawing.Point(110, 19);
            this.newFileNameTextBox.Name = "newFileNameTextBox";
            this.newFileNameTextBox.Size = new System.Drawing.Size(173, 20);
            this.newFileNameTextBox.TabIndex = 23;
            this.newFileNameTextBox.Text = "FileWithMessage";
            // 
            // messageLengthCounter
            // 
            this.messageLengthCounter.AutoSize = true;
            this.messageLengthCounter.Location = new System.Drawing.Point(270, 62);
            this.messageLengthCounter.Name = "messageLengthCounter";
            this.messageLengthCounter.Size = new System.Drawing.Size(13, 13);
            this.messageLengthCounter.TabIndex = 22;
            this.messageLengthCounter.Text = "0";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(149, 61);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(122, 13);
            this.label1.TabIndex = 21;
            this.label1.Text = "Количество символов:";
            // 
            // perceptMessageTextBox
            // 
            this.perceptMessageTextBox.Location = new System.Drawing.Point(14, 89);
            this.perceptMessageTextBox.Multiline = true;
            this.perceptMessageTextBox.Name = "perceptMessageTextBox";
            this.perceptMessageTextBox.ReadOnly = true;
            this.perceptMessageTextBox.Size = new System.Drawing.Size(403, 210);
            this.perceptMessageTextBox.TabIndex = 9;
            this.perceptMessageTextBox.TextChanged += new System.EventHandler(this.perceptMessageTextBox_TextChanged);
            this.perceptMessageTextBox.Leave += new System.EventHandler(this.perceptMessageTextBox_Leave);
            // 
            // perceptMessageBtn
            // 
            this.perceptMessageBtn.Location = new System.Drawing.Point(13, 53);
            this.perceptMessageBtn.Name = "perceptMessageBtn";
            this.perceptMessageBtn.Size = new System.Drawing.Size(130, 28);
            this.perceptMessageBtn.TabIndex = 8;
            this.perceptMessageBtn.Text = "Осадить сообщение";
            this.perceptMessageBtn.UseVisualStyleBackColor = true;
            this.perceptMessageBtn.Click += new System.EventHandler(this.perceptMessageBtn_Click);
            // 
            // ExtractPanel
            // 
            this.ExtractPanel.Controls.Add(this.extractMessageBtn);
            this.ExtractPanel.Controls.Add(this.restoreMessageTextBox);
            this.ExtractPanel.Location = new System.Drawing.Point(15, 113);
            this.ExtractPanel.Name = "ExtractPanel";
            this.ExtractPanel.Size = new System.Drawing.Size(427, 319);
            this.ExtractPanel.TabIndex = 13;
            this.ExtractPanel.Visible = false;
            // 
            // extractMessageBtn
            // 
            this.extractMessageBtn.Location = new System.Drawing.Point(14, 14);
            this.extractMessageBtn.Name = "extractMessageBtn";
            this.extractMessageBtn.Size = new System.Drawing.Size(130, 23);
            this.extractMessageBtn.TabIndex = 18;
            this.extractMessageBtn.Text = "Извлечь сообщение";
            this.extractMessageBtn.UseVisualStyleBackColor = true;
            this.extractMessageBtn.Click += new System.EventHandler(this.extractMessageBtn_Click);
            // 
            // restoreMessageTextBox
            // 
            this.restoreMessageTextBox.Location = new System.Drawing.Point(14, 48);
            this.restoreMessageTextBox.Multiline = true;
            this.restoreMessageTextBox.Name = "restoreMessageTextBox";
            this.restoreMessageTextBox.ReadOnly = true;
            this.restoreMessageTextBox.Size = new System.Drawing.Size(403, 262);
            this.restoreMessageTextBox.TabIndex = 15;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(815, 441);
            this.Controls.Add(this.ExtractPanel);
            this.Controls.Add(this.perciptPanel);
            this.Controls.Add(this.statusListBox);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.filePathTextBox);
            this.Controls.Add(this.loadFileBtn);
            this.Name = "MainForm";
            this.Text = "StegoSVG  Demo";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.perciptPanel.ResumeLayout(false);
            this.perciptPanel.PerformLayout();
            this.ExtractPanel.ResumeLayout(false);
            this.ExtractPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button loadFileBtn;
        private System.Windows.Forms.TextBox filePathTextBox;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.RadioButton precipitationRadio;
        private System.Windows.Forms.RadioButton extractRadio;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ListBox statusListBox;
        private System.Windows.Forms.Panel perciptPanel;
        private System.Windows.Forms.TextBox perceptMessageTextBox;
        private System.Windows.Forms.Button perceptMessageBtn;
        private System.Windows.Forms.Panel ExtractPanel;
        private System.Windows.Forms.Button extractMessageBtn;
        private System.Windows.Forms.TextBox restoreMessageTextBox;
        private System.Windows.Forms.Label messageLengthCounter;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox newFileNameTextBox;
    }
}

