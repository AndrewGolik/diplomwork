﻿using StegoSVG.API;
using StegoSVG.Utils;
using System;
using System.Windows.Forms;

namespace SampleWinForms
{
    public partial class MainForm : Form
    {
        #region Fields

        private string filePath = String.Empty;
        private StegoSvgAPI loadSignSvg = new StegoSvgAPI();

        #endregion

        #region Constructors

        public MainForm()
        {
            InitializeComponent();
        }

        #endregion

        #region Precipitation

        private void perceptMessageBtn_Click(object sender, EventArgs e)
        {
            if (perceptMessageTextBox.Text == String.Empty)
            {
                ShowError("Вы не ввели сообщение.");
                return;
            }
            try
            {
                loadSignSvg.PrecipitationMessage(perceptMessageTextBox.Text, newFileNameTextBox.Text);
                perceptMessageTextBox.ReadOnly = true;
                filePathTextBox.Clear();
                statusListBox.Items.Clear();
                MessageBox.Show("Сообщение было успешно осаждено");
            }
            catch (Exception error)
            {
                ShowError(error.Message);
            }
        }

        private void perceptMessageTextBox_Leave(object sender, EventArgs e)
        {
            if (perceptMessageTextBox.Text!=String.Empty && perceptMessageTextBox.Text[perceptMessageTextBox.Text.Length - 1] != '.')
            {
                perceptMessageTextBox.Text += ".";
            }
        }

        private void perceptMessageTextBox_TextChanged(object sender, EventArgs e)
        {
            TextBox textBox = (TextBox)sender;
            messageLengthCounter.Text = textBox.Text.Length.ToString();
        }

        #endregion

        #region Extract

        private void extractMessageBtn_Click(object sender, EventArgs e)
        {
            try
            {
                restoreMessageTextBox.Text = loadSignSvg.ExtractMessage();
            }
            catch
            {
                ShowError("Не удалось восстановить сообщение. Возможно файл был поврежден");
            }
        }

        #endregion

        #region Common Actions

        private void radio_CheckedChange(object sender, EventArgs e)
        {
            RadioButton radioButton = (RadioButton)sender;
            if (radioButton.Checked)
            {
                perciptPanel.Visible = radioButton.Name == "precipitationRadio" ? true : false;
                ExtractPanel.Visible = radioButton.Name == "extractRadio" ? true : false;
            }
        }

        private void loadFileBtn_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.Cancel)
                return;

            string filename = openFileDialog1.FileName;
            filePathTextBox.Text = filename;
            filePath = filename;
            statusListBox.Items.Clear();
            statusListBox.Items.Add(OutputLine());
            statusListBox.Items.Add($"Начало: {DateTime.Now.ToLongTimeString()}");
            var result = loadSignSvg.Load(filePath);
            statusListBox.Items.Add($"Количество кривых Безье третьего порядка в файле: {result.BezierCount}");
            statusListBox.Items.Add($"Максимальная длина сообщения: {result.MaxMessageLength}");
            statusListBox.Items.Add($"Финиш {DateTime.Now.ToLongTimeString()}");
            statusListBox.Items.Add(OutputLine());
            perceptMessageTextBox.ReadOnly = false;
        }

        #endregion

        #region Outputs

        private string OutputLine()
        {
            return "---------------------------------------------------------------------------------------------------------------";
        }
        #endregion

        #region Error Message
        private void ShowError(string errorMessage)
        {
            MessageBox.Show(errorMessage);
        }
        #endregion
    }
}
