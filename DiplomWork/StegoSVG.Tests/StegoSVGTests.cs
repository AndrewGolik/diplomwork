﻿using NUnit.Framework;
using StegoSVG.API;
using System;
using System.IO;

namespace StegoSVG.Tests
{
    [TestFixture]
    public class StegoSVGTests
    {
        private const string newFileName = "test";
        private string filePathAfterPrecipt = @"C:\folder\" + newFileName + ".svg";
        private string loggerFilePath = @"C:\folder\logger.txt";
        private static readonly string[] filePaths =
        {
            @"C:\folder\1.svg",
            @"C:\folder\2.svg",
            @"C:\folder\3.svg",
            @"C:\folder\4.svg",
            @"C:\folder\5.svg",
            @"C:\folder\6.svg",
            @"C:\folder\7.svg",
            @"C:\folder\8.svg",
            @"C:\folder\9.svg",
            @"C:\folder\10.svg",
            @"C:\folder\11.svg",
        };
        private static readonly string[] simpleMessage =
        {
            "hello.",
            "миррр.",
            "hello миррр."
        };
        private static readonly string[] bigFilePaths = {
             @"C:\folder\1.svg",
              @"C:\folder\2.svg",
              @"C:\folder\3.svg"
        };
        private static readonly string[] hardMessage = {
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum Lorem Ipsum is simply dummy text of the printing and typesetting industry Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took.",
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum Lorem Ipsum is simply dummy text of the printing and typesetting industry Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took Lorem Ipsum is simply dummy text of the printing and typesetting industry Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an последние символы будут русскими для тестирования это пример РаЗнОоБрАзНоГо СоОбЩеНиЯ нА РУССКОМ.",
            "Не следует, однако забывать, что консультация с широким активом требуют определения и уточнения соответствующий условий активизации Равным образом реализация намеченных плановых заданий требуют определения и уточнения соответствующий условий активизации Товарищи! постоянное информационно-пропагандистское обеспечение нашей деятельности требуют определения и уточнения существенных финансовых и административных условий Повседневная практика показывает, что укрепление и развитие структуры позволяет выполнять важные задания по разработке соответствующий условий активизации Разнообразный и богатый опыт реализация намеченных плановых заданий позволяет выполнять важные задания по разработке дальнейших направлений развития Идейные соображения высшего порядка, а также укрепление и развитие структуры представляет собой интересный эксперимент проверки позиций, занимаемых участниками в отношении поставленных задач.",
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged It was poaslarised in the 1960s."
        };
        private static bool success = false;
        private StegoSvgAPI svgAPI;
        private int allTests = 0;
        private int errorTests = 0;
        [OneTimeSetUp]
        public void ClearLogger()
        {
            using (StreamWriter sw = new StreamWriter(loggerFilePath, false, System.Text.Encoding.Default))
            {
                sw.WriteLine("");
            }
        }
        [OneTimeTearDown]
        public void AddTestNumberInfoToLogger()
        {
            Logger("All tests: " + allTests);
            Logger("Success  : " + (allTests - errorTests));
            Logger("Error    : " + errorTests);
        }
        [SetUp]
        public void CreateAPIObject()
        {
            allTests++;
            success = true;
            svgAPI = new StegoSvgAPI();
            Logger("Start   : " + DateTime.Now.ToLongTimeString());
        }
        [TearDown]
        public void EndTest()
        {
            Logger("Status  : " + success.ToString());
            Logger("Finish  : " + DateTime.Now.ToLongTimeString());
            Logger("-----------------------------");
        }
        [Test]
        public void SimpleMessageInAllFile(
            [ValueSource("filePaths")] string filePath,
            [ValueSource("simpleMessage")] string message
            )
        {
            Logger("SIMPLE MESSAGE TEST IN SMALL FILE.");
            svgAPI.Load(filePath);
            string result = "";
            bool preciptResult = false;
            Logger("File    : " + filePath);
            try
            {
                preciptResult = svgAPI.PrecipitationMessage(message, newFileName);
                Assert.AreEqual(true, preciptResult);
                svgAPI.Load(filePathAfterPrecipt);
                result = svgAPI.ExtractMessage();
                Assert.AreEqual(message, result);
            }
            catch (Exception e)
            {
                success = false;
                errorTests++;
                Logger("ERROR");
                Logger(e.Message);
                Logger(message.Length.ToString());
                Logger("Original: " + message);
                Logger("Extract : " + result);
                throw e;
            }
        }
        [Test]
        public void HardLongMessageInBigFile(
            [ValueSource("bigFilePaths")] string filePath,
            [ValueSource("hardMessage")] string message
            )
        {
            Logger("HARD MESSAGE TEST IN BIG FILE.");
            string result = "";
            bool preciptResult = false;
            svgAPI.Load(filePath);
            Logger("File    : " + filePath);
            try
            {
                preciptResult = svgAPI.PrecipitationMessage(message, newFileName);
                Assert.AreEqual(true, preciptResult);
                svgAPI.Load(filePathAfterPrecipt);
                result = svgAPI.ExtractMessage();
                Assert.AreEqual(message, result);
            }
            catch (Exception e)
            {
                errorTests++;
                success = false;
                Logger("ERROR");
                Logger(e.Message);
                Logger(message.Length.ToString());
                Logger("Original: " + message);
                Logger("Extract : " + result);
                throw e;
            }
        }
        public void Logger(string message)
        {
            using (StreamWriter sw = new StreamWriter(loggerFilePath, true, System.Text.Encoding.Default))
            {
                sw.WriteLine(message);
            }
        }
    }
}
