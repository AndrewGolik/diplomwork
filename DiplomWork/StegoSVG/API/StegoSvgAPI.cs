﻿using StegoSVG.Core;
using StegoSVG.Infrastructure;
using StegoSVG.Utils;
using System;
using System.Collections.Generic;
using System.IO;

namespace StegoSVG.API
{
    /// <summary>
    /// Класс, предоставляющий API для работы с библиотекой StegoSVG.
    /// </summary>
    public class StegoSvgAPI : IAPI
    {
        #region Fields
        /// <summary>
        /// Количество обнаруженных в файле кривых.
        /// </summary>
        private int beziersNumber = 0;
        #endregion
        #region Constructors
        /// <summary>
        /// Конструктор по умолчанию.
        /// </summary>
        public StegoSvgAPI() { }
        #endregion
        #region Methods
        /// <summary>
        /// Начало анализа.
        /// </summary>
        public void FileAnalysisStart()
        {
            beziersNumber = 0;
            CommonObjects.ClearAllProperties();
        }
        /// <summary>
        /// Завершение анализа.
        /// </summary>
        public AnalysesDataWrapper FileAnalysisFinish()
        {
            return new AnalysesDataWrapper(
                beziersNumber,
                beziersNumber / 2);
        }
        /// <summary>
        /// Метод для инициализации и анализа SVG-файла.
        /// </summary>
        /// <param name="filePath">Путь к файлу.</param>
        public AnalysesDataWrapper Load(string filePath)
        {
            FileAnalysisStart();
            SVGParser.GetCoordFromSvg(filePath);
            Analyses();
            return FileAnalysisFinish();
        }
        /// <summary>
        /// Метод для инициализации и анализа SVG-файла.
        /// </summary>
        /// <param name="stream">Поток с файлом.</param>
        /// <returns></returns>
        public AnalysesDataWrapper Load(Stream stream)
        {
            FileAnalysisStart();
            SVGParser.GetCoordFromSvg(stream);
            Analyses();
            return FileAnalysisFinish();
        }
        /// <summary>
        /// Метод для анализа загруженного файла.
        /// </summary>
        private void Analyses()
        {
            foreach (var key in CommonObjects.PositionCoordStringInSVG.Keys)
            {
                beziersNumber += Coordinates.GetBeziersByStringCoord(CommonObjects.PositionCoordStringInSVG[key]).Count;
                var tmpBez = Coordinates.GetBeziersByStringCoord(CommonObjects.PositionCoordStringInSVG[key]);
                Coordinates.GetRoundedList(tmpBez, 6);
                CommonObjects.Beziers.Add(key, tmpBez);
            }
        }
        /// <summary>
        /// Метод для осаждения сообщения.
        /// </summary>
        /// <param name="message">Осаждаемое сообщение.</param>
        /// <param name="newFileName">Имя файла с осажденной информацией.</param>
        /// <returns>Возвращает true при успешном осаждении и false в противном случае.</returns>
        public bool PrecipitationMessage(string message, string newFileName)
        {
            try
            {
                Precipt(message);
                SVGParser.LoadCoordToSvg(newFileName);
                return true;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        /// <summary>
        /// Метод для осаждения сообщения.
        /// </summary>
        /// <param name="message">Осаждаемое сообщение.</param>
        /// <returns>Возвращает поток с файлом с осажденной информацией.</returns>
        public MemoryStream PrecipitationMessage(string message)
        {
          //  try
           // {
                Precipt(message);
                return SVGParser.LoadCoordToSvg();
           // }
           // catch (Exception e)
           // {
            //    throw e;
           // }
        }
        /// <summary>
        /// Метод, осаждающий сообщение.
        /// </summary>
        /// <param name="message">Сообщение для осаждения.</param>
        private void Precipt(string message)
        {
            CommonObjects.BeziersWithMess.Clear();
            message += message[message.Length - 1] != '.' ? "." : String.Empty;
            if (message.Length > beziersNumber / 2) throw new Exception("Превышена максимальная длина сообщения");
            string binaryMessage = Message.StringToBinary(message);
            var brokenBinaryMessage = Message.GetBrokenMessage(binaryMessage);
            List<int> beziersKeys = new List<int>();
            foreach (var key in CommonObjects.Beziers.Keys)
            {
                beziersKeys.Add(key);
            }
            int counter = 0;
            for (int i = 0; i < brokenBinaryMessage.Count;)
            {
                int sendRange = CommonObjects.Beziers[beziersKeys[counter]].Count > brokenBinaryMessage.Count - i ?
                    brokenBinaryMessage.Count - i : CommonObjects.Beziers[beziersKeys[counter]].Count * 2;
                if (sendRange > brokenBinaryMessage.Count) sendRange = brokenBinaryMessage.Count;
                if (brokenBinaryMessage.Count - i < sendRange) sendRange = brokenBinaryMessage.Count - i;
                var tmpBez = Precipitation.GetSeparatedBeziers(CommonObjects.Beziers[beziersKeys[counter]],
                    brokenBinaryMessage.GetRange(i, sendRange));
                CommonObjects.BeziersWithMess.Add(beziersKeys[counter], tmpBez);
                i += (CommonObjects.Beziers[beziersKeys[counter]].Count) * 2;
                counter++;
            }
            counter = 0;
            for (int i = 0; i < CommonObjects.BeziersWithMess.Count; i++)
            {
                int key = beziersKeys[counter];
                CommonObjects.PositionCoordStringInSVG[key] = Coordinates.ConvertCoordToString(CommonObjects.BeziersWithMess[key]);
                counter++;
            }
        }
        /// <summary>
        /// Метод для извлечения сообщения.
        /// </summary>
        /// <param name="filePath">Путь к файлу с сообщением.</param>
        /// <returns></returns>
        public string ExtractMessage()
        {
            string binaryMessage = String.Empty;
            string restoreMessage = String.Empty;
            List<int> keys = new List<int>();
            foreach (var key in CommonObjects.PositionCoordStringInSVG.Keys)
            {
                keys.Add(key);
            }
            for (int i = 0; i < CommonObjects.PositionCoordStringInSVG.Count; i++)
            {
                List<PointD> listWithMessage = Coordinates.GetListWithMessage(CommonObjects.PositionCoordStringInSVG[keys[i]]);
                binaryMessage += Extract.GetMessageFromBeziers(listWithMessage);
                if (binaryMessage.Length % 8 == 0)
                {
                    restoreMessage = Message.BinaryToString(binaryMessage);
                    if (restoreMessage.Contains(".")) return restoreMessage.Substring(0, restoreMessage.IndexOf('.') + 1);
                }
            }
            while (binaryMessage.Length % 8 != 0) binaryMessage = binaryMessage.Substring(0, binaryMessage.Length - 1);
            restoreMessage = Message.BinaryToString(binaryMessage);
            if (restoreMessage.IndexOf('.') != -1) return restoreMessage.Substring(0, restoreMessage.IndexOf('.') + 1);
            return restoreMessage;
        }
        #endregion
    }
}
