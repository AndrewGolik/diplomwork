﻿using StegoSVG.Infrastructure;
using System;
using System.Collections.Generic;

namespace StegoSVG.BezierCurve
{
    /// <summary>
    /// Класс для работы с кубическими кривыми Безье.
    /// </summary>
    internal static class CubicBezierCurve
    {
        #region Methods
        /// <summary>
        /// Внутренний метод для разделения кубической кривой Безье в заданном отношении.
        /// </summary>
        /// <param name="t">Отношение, в котором будет разделена кривая.</param>
        /// <param name="start">Первая опорная точка.</param>
        /// <param name="control1">Первая контрольная точка.</param>
        /// <param name="control2">Вторая контрольная точка.</param>
        /// <param name="end">Вторая опорная точка.</param>
        /// <returns>Возвращает координаты точки, которая делит исходную кривую в заданном отношении.</returns>
        private static PointD GetDivisionPointCubic(double relation, PointD start, PointD control1, 
            PointD control2, PointD end)
        {
            double x = Math.Pow(1 - relation, 3) * start.X + 3 * relation * Math.Pow(1 - relation, 2) * control1.X + 3 * Math.Pow(relation, 2) * (1 - relation) * control2.X + Math.Pow(relation, 3) * end.X;
            double y = Math.Pow(1 - relation, 3) * start.Y + 3 * relation * Math.Pow(1 - relation, 2) * control1.Y + 3 * Math.Pow(relation, 2) * (1 - relation) * control2.Y + Math.Pow(relation, 3) * end.Y;
            return new PointD(x, y);
        }
        /// <summary>
        /// Метод для расчета разделения кривой на две части в заданном отношении.
        /// </summary>
        /// <param name="t">Отношение, в котором будет разделена кривая.</param>
        /// <param name="bezier">Кривая Безье, заданная списком точек PointD</param>
        /// <returns></returns>
        internal static (PointD[], PointD[]) DrawTwoCubicBezier(double t, List<PointD> bezier)
        {
            return DrawTwoCubicBezier(t,bezier[0],bezier[1],bezier[2],bezier[3]);
        }
        /// <summary>
        /// Метод для расчета разделения кривой на две части в заданном отношении.
        /// </summary>
        /// <param name="t">Отношение, в котором будет разделена кривая.</param>
        /// <param name="start">Первая опорная точка.</param>
        /// <param name="control1">Первая контрольная точка.</param>
        /// <param name="control2">Вторая контрольная точка.</param>
        /// <param name="end">Вторая опорная точка.</param>
        /// <returns>Возвращает кортеж из двух кубических кривых Безье в формате PointD[].</returns>
        internal static (PointD[], PointD[]) DrawTwoCubicBezier(double t, PointD start, PointD control1, 
            PointD control2, PointD end)
        {
            double tt = 1 - t;
            PointD startFirst = start;
            PointD endFirst = GetDivisionPointCubic(t, start, control1, control2, end);
            double tmpX = start.X * tt + control1.X * t;
            double tmpY = start.Y * tt + control1.Y * t;
            PointD control1First = new PointD(Math.Round(tmpX, 6), Math.Round(tmpY, 6)); // первая контрольная точка
            double xx = control1.X * tt + control2.X * t;
            double yy = control1.Y * tt + control2.Y * t;
            tmpX = control1First.X * tt + xx * t;
            tmpY = control1First.Y * tt + yy * t;
            PointD control2First = new PointD(Math.Round(tmpX, 6), Math.Round(tmpY, 6)); // вторая контрольная точка
            endFirst = new PointD(Math.Round(endFirst.X, 6), Math.Round(endFirst.Y, 6));
            PointD[] bezierFirstPoints ={startFirst, control1First, control2First, endFirst};
            PointD startSecond = endFirst;
            PointD endSecond = end;
            tmpX = control2.X * tt + end.X * t;
            tmpY = control2.Y * tt + end.Y * t;
            PointD control2Second = new PointD(Math.Round(tmpX, 6), Math.Round(tmpY, 6));
            tmpX = control2Second.X * t + xx * tt;
            tmpY = control2Second.Y * t + yy * tt;
            PointD control1Second = new PointD(Math.Round(tmpX, 6), Math.Round(tmpY, 6));
            PointD[] bezierSecondPoints ={startSecond, control1Second, control2Second, endSecond};
            return (bezierFirstPoints, bezierSecondPoints);
        }
        #endregion
    }
}
