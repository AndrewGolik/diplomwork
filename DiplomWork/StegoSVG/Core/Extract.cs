﻿using StegoSVG.Infrastructure;
using System;
using System.Collections.Generic;

namespace StegoSVG.Core
{
    /// <summary>
    /// Класс для извлечения сообщения.
    /// </summary>
    internal static class Extract
    {
        #region Methods
        /// <summary>
        /// Метод для получения сообщения из списка точек.
        /// </summary>
        /// <param name="listWithMessage">Список точек кривых Безье.</param>
        /// <returns>Возвращает осажденное сообщение.</returns>
        internal static string GetMessageFromBeziers(List<PointD> listWithMessage)
        {
            string message = string.Empty;
            for (int i = 0; i < listWithMessage.Count; i += 3)
            {
                var part1 = listWithMessage[i].X.ToString();
                var part2 = listWithMessage[i].Y.ToString();
                string firstNumber = part1[part1.Length - 1].ToString();
                string secondNumber = part2[part2.Length - 1].ToString();
                string relation = firstNumber + secondNumber;
                message += GetPartByRealtion(int.Parse(relation));
            }
            return message;
        }
        /// <summary>
        /// Метод для преобразования полученного из коордитан отношения в бинарную пару.
        /// </summary>
        /// <param name="relation">Полученное отношение.</param>
        /// <returns>Возвращает бинарную пару.</returns>
        private static string GetPartByRealtion(int relation)
        {
            if (relation > 0 & relation < 25)
            {
                return "11";
            }
            else if (relation >= 25 & relation < 50)
            {
                return "10";
            }
            else if (relation >= 50 & relation < 75)
            {
                return "01";
            }
            else if (relation >= 75 & relation <= 100)
            {
                return "00";
            }
            else
            {
                throw new Exception("Передано некорректное отношение. Возможно сообщение было повреждено.");
            }
        }
        #endregion
    }
}
