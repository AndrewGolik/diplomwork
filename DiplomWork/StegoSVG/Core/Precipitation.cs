﻿using StegoSVG.BezierCurve;
using StegoSVG.Infrastructure;
using StegoSVG.Utils;
using System;
using System.Collections.Generic;
using System.Linq;

namespace StegoSVG.Core
{
    /// <summary>
    /// Класс для формирования списка разделенных кривых Безье и списка осаждаемых отношений.
    /// </summary>
    internal static class Precipitation
    {
        #region Methods
        /// <summary>
        /// Метод для получения списка с разделенными кривыми Безье и формирования списка осаждаемых отношений.
        /// </summary>
        /// <param name="beziers">Исходный список кривых.</param>
        /// <param name="brokenMessage">Список бинарных пар осаждаемого сообщения.</param>
        /// <returns>Возвращает список с разделенными кривыми Безье.</returns>
        internal static List<List<PointD>> GetSeparatedBeziers(List<List<PointD>> beziers,
            List<string> brokenMessage)
        {
            var returnBeziers = new List<List<PointD>>();
            Coordinates.GetRoundedList(beziers, 6);
            var brokenSignLength = brokenMessage.Count();
            int bezierCount = 0;
            for (int i = 0; i < brokenSignLength; i += 2)
            {
                double divisionRelative = 0.0;
                divisionRelative = GetRelation(brokenMessage[i]) > GetRelation(brokenMessage[i + 1]) ? 0.25 : 0.75;
                var (firstBezier, secondBezier) = CubicBezierCurve.DrawTwoCubicBezier(divisionRelative, beziers[bezierCount]);
                AddRelToList(GetRelation(brokenMessage[i]));
                AddRelToList(GetRelation(brokenMessage[i + 1]));
                returnBeziers.Add(firstBezier.ToList());
                returnBeziers.Add(secondBezier.ToList());
                bezierCount++;

            }
            var tmp = new List<List<PointD>>(beziers);
            tmp.RemoveRange(0, brokenSignLength / 2);
            returnBeziers.AddRange(tmp);
            return returnBeziers;
        }
        /// <summary>
        /// Метод для разбиения и добавления отношения в список для осаждения.
        /// </summary>
        /// <param name="relation">Разбиваемое отношение.</param>
        private static void AddRelToList(double relation)
        {
            var (unused, hideRel) = GetPartsCP(relation);
            CommonObjects.PartRealtions.Add(hideRel[0]);
            CommonObjects.PartRealtions.Add(hideRel[1]);
        }
        /// <summary>
        /// Метод для разделения отношения на целую и дробную части. 
        /// </summary>
        /// <param name="relation">Разделяемое отношение.</param>
        /// <returns>Возвращает кортеж из двух строк, первая строка целая часть отношения, вторая - дробная.</returns>
        private static (string, string) GetPartsCP(double relation)
        {
            string stringRelation;
            stringRelation = relation.ToString();
            var intPart = stringRelation.Split(new char[] { ',', '.' })[0];
            var doublePart = stringRelation.Split(new char[] { ',', '.' })[1];
            if (doublePart.Length < 2)
            {
                while (doublePart.Length == 1)
                {
                    doublePart += "0";
                }
            }
            return (intPart, doublePart);
        }
        /// <summary>
        /// Метод преобразует бинарную пару в отношение.
        /// </summary>
        /// <param name="partOfBinMess">Бинарная пара.</param>
        /// <returns>Возвращает отношение, соответствующее бинарной паре.</returns>
        private static double GetRelation(string partOfBinMess)
        {
            switch (partOfBinMess)
            {
                case "11":              //0.01-0.25;
                    {
                        return 0.12;
                    }
                case "10":              //0.26-0.50;
                    {
                        return 0.37;
                    }
                case "01":              //0.51-.75
                    {
                        return 0.64;
                    }
                case "00":              //0.76-0.99
                    {
                        return 0.87;
                    }
                default:                //Если в метод передана не бинарная пара.
                    {
                        throw new Exception("Передана некорректная бинарная пара. Возможно сообщение было повреждено.");
                    }
            }
        }
        /// <summary>
        /// Метод для осаждения отношений в список координат.
        /// </summary>
        /// <param name="list">Список координат.</param>
        internal static void PerciptRelationInCoordList(List<string> list)
        {
            int y = 2;
            for (int i = CommonObjects.LastPosition; i < CommonObjects.PartRealtions.Count; i += 2)
            {
                if (y >= list.Count)
                {
                    CommonObjects.LastPosition = i;
                    return;
                }
                if (list[y].IndexOf(',') == -1) list[y] += ',';
                if (list[y + 1].IndexOf(',') == -1) list[y + 1] += ',';
                list[y] += CommonObjects.PartRealtions[i];
                list[y + 1] += CommonObjects.PartRealtions[i + 1];
                y += 6;
                CommonObjects.LastPosition = i + 2;
            }
        }
        #endregion
    }
}
