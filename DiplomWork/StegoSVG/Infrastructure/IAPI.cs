﻿using StegoSVG.Utils;

namespace StegoSVG.Infrastructure
{
    /// <summary>
    /// Интерфейс для библиотеки StegoSVG.
    /// </summary>
    public interface IAPI
    {
        /// <summary>
        /// Метод для инициализации библиотеки и анализа файла.
        /// </summary>
        /// <param name="filePath">Путь к файлу.</param>
        AnalysesDataWrapper Load(string filePath);
        /// <summary>
        /// Метод для осаждения сообщения.
        /// </summary>
        /// <param name="message">Осаждаемое сообщение.</param>
        /// <param name="newFileName">Новое имя файла.</param>
        /// <returns>Возвращает булево значение успеха или ошибки.</returns>
        bool PrecipitationMessage(string message, string newFileName);
        /// <summary>
        /// Метод для извлечения сообщения.
        /// </summary>
        /// <returns>Возвращает извлеченное сообщение в бинарном или восстановленном виде.</returns>
        string ExtractMessage();
    }
}
