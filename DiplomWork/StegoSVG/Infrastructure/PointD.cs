﻿namespace StegoSVG.Infrastructure
{
    /// <summary>
    /// Структура для представления точек.
    /// </summary>
    public struct PointD
    {
        #region Properties
        /// <summary>
        /// X-координата.
        /// </summary>
        public double X { get; set; }
        /// <summary>
        /// Y-координата.
        /// </summary>
        public double Y { get; set; }
        #endregion
        #region Contructors
        /// <summary>
        /// Конструктор из double.
        /// </summary>
        /// <param name="x">X-координата.</param>
        /// <param name="y">Y-координата.</param>
        public PointD(double x, double y)
        {
            X = x; Y = y;
        }
        /// <summary>
        /// Конструктор из string.
        /// </summary>
        /// <param name="x">X-координата.</param>
        /// <param name="y">Y-координата.</param>
        public PointD(string x, string y)
        {
            x = x.Replace('.', ','); y = y.Replace('.', ',');
            X = double.Parse(x); Y = double.Parse(y);
        }
        #endregion
        #region Methods
        /// <summary>
        /// Переопределенная версия метода ToString.
        /// </summary>
        /// <returns>Возвращает строку с координатами.</returns>
        public override string ToString()
        {
            return X.ToString() + " " + Y.ToString();
        }
        #endregion
    }
}
