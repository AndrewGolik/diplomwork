﻿using System;

namespace StegoSVG.Utils
{
    /// <summary>
    /// Класс для обертки параметров событий.
    /// </summary>
    public class AnalysesDataWrapper
    {
        #region Properties
        /// <summary>
        /// Количество кривых Безье в файле.
        /// </summary>
        public int BezierCount { get; } = 0;
        /// <summary>
        /// Максимальная длина сообщения.
        /// </summary>
        public int MaxMessageLength { get; } = 0;
        #endregion
        #region Constructors
        /// <summary>
        /// Конструктор с параметрами.
        /// </summary>
        /// <param name="bezierCount">Количество кривых Безье.</param>
        /// <param name="maxLengthMessage">Максимальная длина сообщения.</param>
        public AnalysesDataWrapper(int bezierCount, int maxLengthMessage)
        {
            BezierCount = bezierCount;
            MaxMessageLength = maxLengthMessage;
        }
        /// <summary>
        /// Конструктор по умолчанию.
        /// </summary>
        public AnalysesDataWrapper() { }
        #endregion
    }
}
