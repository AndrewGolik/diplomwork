﻿using StegoSVG.Infrastructure;
using System.Collections.Generic;

namespace StegoSVG.Utils
{
    /// <summary>
    /// Класс для хранения общих объектов.
    /// </summary>
    internal static class CommonObjects
    {
        #region Properties
        /// <summary>
        /// Список осаждаемых отношений.
        /// </summary>
        internal static List<char> PartRealtions { get; set; } = new List<char>();
        /// <summary>
        /// Переменная, указывающая, закрыт ли путь в SVG. 
        /// </summary>
        internal static bool ClosePath { get; set; } = false;
        internal static int LastPosition { get; set; } = 0;
        /// <summary>
        /// Словарь для хранения позиции и значения тэга Path в SVG.
        /// </summary>
        internal static Dictionary<int, string> PositionCoordStringInSVG { get; set; } = new Dictionary<int, string>();
        /// <summary>
        /// Словарь для хранения кривых Безье.
        /// </summary>
        internal static Dictionary<int, List<List<PointD>>> Beziers { get; set; } = new Dictionary<int, List<List<PointD>>>();
        /// <summary>
        /// Словарь для хранения кривых Безье с осажденным сообщением.
        /// </summary>
        internal static Dictionary<int, List<List<PointD>>> BeziersWithMess { get; set; }
            = new Dictionary<int, List<List<PointD>>>();
        #endregion
        #region Methods
        internal static void ClearAllProperties()
        {
            LastPosition = 0;
            PartRealtions.Clear();
            PositionCoordStringInSVG.Clear();
            Beziers.Clear();
            BeziersWithMess.Clear();
        }
        #endregion
    }
}
