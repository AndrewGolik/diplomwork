﻿using StegoSVG.Core;
using StegoSVG.Infrastructure;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace StegoSVG.Utils
{
    /// <summary>
    /// Класс для преобразования координат.
    /// </summary>
    internal static class Coordinates
    {
        #region Methods
        /// <summary>
        /// Метод добавляет пробелы перед отрицательными значениями.
        /// </summary>
        /// <param name="coordString">Изменяемая строка.</param>
        /// <returns>Возвращает измененную строку.</returns>
        internal static string AddSpaceBeforeMinus(string coordString)
        {
            string replacedString = coordString.Replace("-", " -");

            string pattern = @"\s+";
            string replacement = " ";
            return Regex.Replace(replacedString, pattern, replacement);
        }

        /// <summary>
        /// Метод округляет координаты в списке с указанной точностью.
        /// </summary>
        /// <param name="lists">Список координат.</param>
        /// <param name="accuracy">Количество знаков после запятой.</param>
        internal static void GetRoundedList(List<List<PointD>> lists, int accuracy)
        {
            for (int i = 0; i < lists.Count; i++)
            {
                for (int y = 0; y < 4; y++)
                {
                    lists[i][y] = new PointD(
                        Math.Round(lists[i][y].X, accuracy),
                        Math.Round(lists[i][y].Y, accuracy)
                        );
                }
            }
        }

        /// <summary>
        /// Метод для разбиения строки относительных координат на список по точкам. 
        /// </summary>
        /// <param name="coordString">Строка с координатами.</param>
        /// <returns>Возвращает список точек с относительными координатами.</returns>
        internal static List<PointD> GetRelativePoints(string coordString)
        {
            List<PointD> points = new List<PointD>();
            string[] coords = coordString.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < coords.Length; i += 2)
            {
                try
                {
                    points.Add(new PointD(coords[i], coords[i + 1]));
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
            return points;
        }

        /// <summary>
        /// Метод для разбиения списка относительных координат на кубические кривые Безье.
        /// </summary>
        /// <param name="relativePoints">Список относительных координат.</param>
        /// <param name="startPoint">Первая опорная точка.</param>
        /// <returns>Возвращает список с кривыми Безье в абсолютных координатах.</returns>
        internal static List<List<PointD>> GetBezierCurve(List<PointD> relativePoints, PointD startPoint)
        {
            List<List<PointD>> bezierLists = new List<List<PointD>>();
            for (int i = 0; i < relativePoints.Count; i += 3)
            {
                List<PointD> tmpBezier = new List<PointD> { startPoint };
                for (int y = i; y < i + 3; y++)
                {
                    PointD tmpPoint = new PointD(startPoint.X + relativePoints[y].X, startPoint.Y + relativePoints[y].Y);
                    tmpBezier.Add(tmpPoint);
                }
                startPoint = tmpBezier[3];
                bezierLists.Add(tmpBezier);
            }
            return bezierLists;
        }

        /// <summary>
        /// Метод переводит список с кривыми Безье в относительные координаты.
        /// </summary>
        /// <param name="beziersWithMessage">Список с кривыми Безье (с абсолютными координатами).</param>
        /// <returns>Строку для выгрузки в SVG файл.</returns>
        internal static string ConvertCoordToString(List<List<PointD>> beziersWithMessage)
        {
            List<string> coordRelative = CreateCoordRelativeList(beziersWithMessage);

            Precipitation.PerciptRelationInCoordList(coordRelative);

            return CreateReturnStringWithCoord(coordRelative);
        }

        /// <summary>
        /// Метод для перевода списка кривых Безье в абсолютных координатах в список относительных координат.
        /// </summary>
        /// <param name="beziersWithMessage">Список кривых Безье.</param>
        /// <returns>Возврщает список относительных координат.</returns>
        private static List<string> CreateCoordRelativeList(List<List<PointD>> beziersWithMessage)
        {
            List<string> coordRelative = new List<string>{beziersWithMessage[0][0].X.ToString(),beziersWithMessage[0][0].Y.ToString()
            };
            for (int i = 0; i < beziersWithMessage.Count; i++)
            {
                for (int y = 1; y < 4; y++)
                {
                    if (y == 1)
                    {
                        var result = beziersWithMessage[i][y].X - beziersWithMessage[i][0].X;
                        if (result.ToString().Contains("E")){result = 0.00001;}
                        coordRelative.Add(Math.Round(result, 5).ToString());  
                        result = beziersWithMessage[i][y].Y - beziersWithMessage[i][0].Y;
                        if (result.ToString().Contains("E")){result = 0.0001;}
                        coordRelative.Add(Math.Round(result, 5).ToString()); 
                    }
                    else
                    {
                        coordRelative.Add(Math.Round((beziersWithMessage[i][y].X - beziersWithMessage[i][0].X), 6).ToString("F6"));
                        coordRelative.Add(Math.Round((beziersWithMessage[i][y].Y - beziersWithMessage[i][0].Y), 6).ToString("F6"));
                    }
                }
            }
            return coordRelative;
        }

        /// <summary>
        /// Формирование строки координат.
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        private static string CreateReturnStringWithCoord(List<string> list)
        {
            string returnString = "m" + list[0] + " " + list[1] + "c";
            for (int i = 2; i < list.Count; i++)
            {
                returnString += " " + list[i];
            }
            if (CommonObjects.ClosePath)
            {
                returnString += "z";
            }
            return returnString.Replace(',', '.').Replace("c ", "c");
        }

        /// <summary>
        /// Метод для перевода строки координат в список кривых Безье.
        /// </summary>
        /// <param name="coordString">Строка с координатами.</param>
        /// <returns>Возвращает список кривых Безье.</returns>
        internal static List<List<PointD>> GetBeziersByStringCoord(string coordString)
        {
            try
            {
                var (startPoint, bezierCoord) = ParseCoordString(coordString);
                var relativePoints = GetRelativePoints(bezierCoord);
                var bezier = GetBezierCurve(relativePoints, startPoint);
                return bezier;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Метод для преобразования строки координат в список точек.
        /// </summary>
        /// <param name="coordString">Строка координат.</param>
        /// <returns>Список кривых Безье.</returns>
        internal static List<PointD> GetListWithMessage(string coordString)
        {
            List<PointD> relativePoints = new List<PointD>();
            try
            {
                var (startPoint, bezierCoord) = ParseCoordString(coordString);
                relativePoints = GetRelativePoints(bezierCoord);
                return relativePoints;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Метод для разделения строки координат на стартовую точку и строку с координатами.
        /// </summary>
        /// <param name="coordString">Строка с координатами.</param>
        /// <returns>Возвращает кортеж из стартовой точки PointD и строки координат.</returns>
        private static (PointD, string) ParseCoordString(string coordString)
        {
            coordString = coordString.Replace("M", "m");
            string tmp = AddSpaceBeforeMinus(coordString); tmp = tmp.Trim(); tmp = tmp.Replace(',', ' ');
            string startCoord = tmp.Substring(tmp.IndexOf('m') + 1, tmp.IndexOf('c') - 1 - tmp.IndexOf('m'));
            try
            {
                startCoord = startCoord.Trim(); startCoord = startCoord.Replace(',', ' ');
                var test = startCoord.Split(' '); var x = startCoord.Split(' ')[0]; var y = startCoord.Split(' ')[1];
                PointD startPoint = new PointD(x, y); string bezierCoord = String.Empty;
                if (tmp[tmp.Length - 1] == 'z')
                {
                    bezierCoord = tmp.Substring(tmp.IndexOf('c') + 1, tmp.IndexOf('z') - 1 - tmp.IndexOf('c')); CommonObjects.ClosePath = true;
                }
                else
                {
                    bezierCoord = tmp.Substring(tmp.IndexOf('c') + 1, tmp.Length - 1 - tmp.IndexOf('c')); CommonObjects.ClosePath = false;
                }
                return (startPoint, bezierCoord);
            }
            catch (Exception e) { throw e; };
        }
        #endregion
    }
}
