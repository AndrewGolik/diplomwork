﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StegoSVG.Utils
{
    /// <summary>
    /// Класс для преобразований сообщения.
    /// </summary>
    internal static class Message
    {
        #region Methods
        /// <summary>
        /// Метод для разделения строки с бинарным представлением сообщения в список бинарных пар.
        /// </summary>
        /// <param name="binaryMessage">Строка с бинарным представлением сообщения.</param>
        /// <returns>Список бинарных пар.</returns>
        internal static List<string> GetBrokenMessage(string binaryMessage)
        {
            List<string> returnList = new List<string>();
            for (int i = 0; i < binaryMessage.Length; i += 2)
            {
                returnList.Add(binaryMessage.Substring(i, 2));
            }
            return returnList;
        }

        /// <summary>
        /// Метод преобразует сообщение в строку с бинарным представлением.
        /// </summary>
        /// <param name="convert">Конвертируемое сообщение.</param>
        /// <returns>Возвращает строку с бинарным представлением сообщения.</returns>
        internal static string StringToBinary(string convert)
        {
            byte[] test = Encoding.GetEncoding(1251).GetBytes(convert);
            var returnString = "";
            for (int i = 0; i < test.Length; i++)
            {
                var tmp = Convert.ToString(test[i], 2);
                while (tmp.Length < 8)
                {
                    tmp = '0' + tmp;
                }
                returnString += tmp;
            }
            return returnString;
        }

        /// <summary>
        /// Метод преобразует строку с бинарным представлением в сообщение.
        /// </summary>
        /// <param name="binary">Строка с бинарным представлением сообщения.</param>
        /// <returns>Вовращает восстановленное сообщение.</returns>
        internal static string BinaryToString(string binary)
        {
            List<byte> lst = new List<byte>();
            int position = 0;
            string returnString = "";
            while (position < binary.Length)
            {
                lst.Add(Convert.ToByte(binary.Substring(position, 8), 2));
                position += 8;
            }
            returnString = Encoding.GetEncoding(1251).GetString(lst.ToArray());
            return returnString;
        }
        #endregion
    }
}
