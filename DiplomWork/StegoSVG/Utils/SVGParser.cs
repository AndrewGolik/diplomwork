﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;

namespace StegoSVG.Utils
{
    /// <summary>
    /// Класс для разбора файлов формата SVG.
    /// </summary>
    internal static class SVGParser
    {
        #region Fields
        private static XmlDocument document;
        #endregion
        #region Properties
        /// <summary>
        /// Свойство для хранения информации о файле.
        /// </summary>
        internal static FileInfo FileInfo { get; private set; }
        #endregion
        #region Constructors
        static SVGParser()
        {
            document = new XmlDocument();
        }
        #endregion
        #region Methods
        /// <summary>
        /// Метод для разбора файла формата SVG.
        /// </summary>
        /// <param name="filePath">Путь к файлу.</param>
        internal static void GetCoordFromSvg(string filePath)
        {
            FileInfo fileInfo = new FileInfo(filePath);
            SVGParser.FileInfo = fileInfo;
            CommonObjects.PositionCoordStringInSVG = new Dictionary<int, string>();
            document.Load(filePath);
            ExtractCoordFromSvg(document.DocumentElement);
        }
        /// <summary>
        /// Метод для разбора файла формата SVG.
        /// </summary>
        /// <param name="stream">Поток с файлом.</param>
        internal static void GetCoordFromSvg(Stream stream)
        {
            document.Load(stream);
            ExtractCoordFromSvg(document.DocumentElement);
        }
        /// <summary>
        /// Получает из файла кривые Безье третьего порядка.
        /// </summary>
        /// <param name="root">Корневой элемент разбираемого файла.</param>
        private static void ExtractCoordFromSvg(XmlNode root)
        {
            int counter = 0;
            foreach (XmlNode node in root)
            {
                if (node.Name == "g")
                {
                    foreach (XmlNode path in node)
                    {
                        if (path.Name == "path")
                        {
                            var coordString = path.Attributes.GetNamedItem("d").Value;
                            int mCount = coordString.ToCharArray().Where(c => (c == 'm') ||(c=='M') ).Count();
                            int cCount = coordString.ToCharArray().Where(c => c == 'c').Count();
                            int bigCCount = coordString.ToCharArray().Where(c => c == 'C').Count();
                            int zCount = coordString.ToCharArray().Where(c => (c == 'z')||(c=='Z')).Count();
                            int hCount = coordString.ToCharArray().Where(c => (c == 'h')||(c=='H')).Count();
                            int vCount = coordString.ToCharArray().Where(c => (c == 'v')||(c=='V')).Count();
                            int eCount = coordString.ToCharArray().Where(c => (c == 'e') || (c == 'E')).Count();
                            int sCount = coordString.ToCharArray().Where(c => (c == 's') || (c == 'S')).Count();
                            int qCount = coordString.ToCharArray().Where(c => (c == 'q') || (c == 'Q')).Count();
                            int err = coordString.ToCharArray().Where(c => (c == 'l') || (c == 'l')).Count();
                            if (mCount == 1 & cCount == 1 & zCount < 2 & hCount == 0 & vCount == 0 & bigCCount == 0 & eCount == 0 & sCount==0 & qCount==0 & err == 0)
                                CommonObjects.PositionCoordStringInSVG.Add(counter, coordString);
                        }
                        counter++;
                    }
                } else if(node.Name == "path")
                {
                    var coordString = node.Attributes.GetNamedItem("d").Value;
                    int mCount = coordString.ToCharArray().Where(c => (c == 'm') || (c == 'M')).Count();
                    int cCount = coordString.ToCharArray().Where(c => c == 'c').Count();
                    int bigCCount = coordString.ToCharArray().Where(c => c == 'C').Count();
                    int zCount = coordString.ToCharArray().Where(c => (c == 'z') || (c == 'Z')).Count();
                    int hCount = coordString.ToCharArray().Where(c => (c == 'h') || (c == 'H')).Count();
                    int vCount = coordString.ToCharArray().Where(c => (c == 'v') || (c == 'V')).Count();
                    int eCount = coordString.ToCharArray().Where(c => (c == 'e') || (c == 'E')).Count();
                    int sCount = coordString.ToCharArray().Where(c => (c == 's') || (c == 'S')).Count();
                    int qCount = coordString.ToCharArray().Where(c => (c == 'q') || (c == 'Q')).Count();
                    int err = coordString.ToCharArray().Where(c => (c == 'l') || (c == 'l')).Count();
                    if (mCount == 1 & cCount == 1 & zCount < 2 & hCount == 0 & vCount == 0 & bigCCount == 0 & eCount == 0 & sCount == 0 & qCount == 0 & err == 0)
                        CommonObjects.PositionCoordStringInSVG.Add(counter, coordString);
                    counter++;
                }
            }
        }
        /// <summary>
        /// Метод для загрузки координат в файл формата SVG.
        /// </summary>
        /// <param name="newFileName">Имя создаваемого файла.</param>
        internal static void LoadCoordToSvg(string newFileName)
        {
            Load();
            string newFilePath = FileInfo.Directory + "\\" + newFileName + ".svg";
            document.Save(newFilePath);
        }
        /// <summary>
        /// Метод для загрузки координат в файл формата SVG.
        /// </summary>
        /// <returns>Поток с файлом.</returns>
        internal static MemoryStream LoadCoordToSvg()
        {
            Load();
            MemoryStream stream = new MemoryStream();
            document.Save(stream);
            return stream;
        }
        /// <summary>
        /// Загружает координаты с осажденной информацией в файл.
        /// </summary>
        private static void Load()
        {
            int counter = 0;
            XmlNode root = document.DocumentElement;
            foreach (XmlNode node in root)
            {
                if (node.Name == "g")
                {
                    foreach (XmlNode path in node)
                    {
                        if (path.Name == "path")
                        {
                            if (CommonObjects.PositionCoordStringInSVG.Keys.Contains(counter))
                            {
                                path.Attributes.GetNamedItem("d").Value = CommonObjects.PositionCoordStringInSVG[counter];
                            }
                        }
                        counter++;
                    }
                } else if(node.Name == "path")
                {
                    if (CommonObjects.PositionCoordStringInSVG.Keys.Contains(counter))
                    {
                        node.Attributes.GetNamedItem("d").Value = CommonObjects.PositionCoordStringInSVG[counter];
                    }
                    counter++;
                }
            }
        }
        #endregion
    }
}
